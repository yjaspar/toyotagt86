ToyotaConcoursPhotoGt86::Application.routes.draw do
  get "subscribes" => 'subscribes#index'
  get "subscribes/like" => 'subscribes#like'
  get "subscribes/fbid/:id" => 'subscribes#get_by_fbid'
  get "subscribes/wrapper" => 'subscribes#wrapper'
  get "subscribes/picture/:id" => 'subscribes#picture'
  get "subscribes/:id" => 'subscribes#get'
  get "subscribes/:pic/:page/:status" => 'subscribes#fetch'
  get "subscribes/:pic/:page" => 'subscribes#fetch'

  post "subscribes" => 'subscribes#create'
  post "subscribes/form" => 'subscribes#form'
  post "subscribes/:id/vote" => 'subscribes#vote'

  get "session" => 'sessions#connect'
  get "session/callback" => 'sessions#callback'

  root :to => 'subscribes#index'

  get "admin" => "admins#index"

  get "admin/subscribers" => "admins#subscribers"
  get "admin/subscribers/:page" => "admins#subscribers"
  post 'admin/subscribers/moderate' => 'admins#moderate'

  get 'admin/winners' => 'admins#winners'
  get 'admin/winners/:page' => 'admins#winners'
  post 'admin/winners/grant' => 'admins#grant'
  post 'admin/winners/ungrant' => 'admins#ungrant'

  get 'admin/notifications' => 'admins#notifications'

  get 'admin/users' => 'admins#users'
  get 'admin/stats' => 'admins#get_stats'
  post 'admin/users' => 'admins#create'
  delete 'admin/users' => 'admins#destroy'
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
