class Subscribe
	include MongoMapper::Document

	key :fb_userid, String
	key :last_name, String
	key :first_name, String
	key :email, String
	key :adress, String
	key :zip_code, String
	key :city, String
	key :fb_userid, String
	key :category_1, Array
	key :category_2, Array
	key :category_3, Array
	timestamps!

	many :pictures, :in => :category_1
	many :pictures, :in => :category_2
	many :pictures, :in => :category_3

	def self.get(id, syst=false)
		raw = Subscribe.where({
			'$or' => [
				{'category_1' => {'$elemMatch' => {'id' => id}}},
				{'category_2' => {'$elemMatch' => {'id' => id}}},
				{'category_3' => {'$elemMatch' => {'id' => id}}},
			]
		})

		raw.each do |r|
			r['category_1'].each { |a| return {:id => a['id'], :full_name => r.first_name+' '+r.last_name.truncate(20), :profile_picture => 'https://graph.facebook.com/'+r.fb_userid+'/picture', :picture => Settings.root_path+'subscribes/picture/'+a['id']} if a['id'] = id }
			r['category_2'].each { |a| return {:id => a['id'], :full_name => r.first_name+' '+r.last_name.truncate(20), :profile_picture => 'https://graph.facebook.com/'+r.fb_userid+'/picture', :picture => Settings.root_path+'subscribes/picture/'+a['id']} if a['id'] = id }
			r['category_3'].each { |a| return {:id => a['id'], :full_name => r.first_name+' '+r.last_name.truncate(20), :profile_picture => 'https://graph.facebook.com/'+r.fb_userid+'/picture', :picture => Settings.root_path+'subscribes/picture/'+a['id']} if a['id'] = id }
		end
		return object
	end

	def self.winners(num, syst = false)
		@obj = {:size => 0, :data => []}
		tmp = Subscribe.where({'category_'+num => {'$elemMatch' => {'won' => true}}})

		tmp.each do |p|
    		p['category_'+num].each do |e| 
    			if e['won'] == true
	    			@obj[:data][@obj[:size]] = {:id => e['id'], :full_name => (p.first_name+' '+p.last_name).truncate(20), :profile_picture => 'https://graph.facebook.com/'+p.fb_userid+'/picture', :picture => Settings.root_path+'subscribes/picture/'+e['id'], :category => ('category_'+num.to_s).gsub("category_1", "MA TOYOTA").gsub("category_2", "DETAIL EXTERIEUR").gsub("category_3", "DETAIL INTERIEUR")}

	    			if syst && e['status'] == true
				    	@obj[:data][@obj[:size]][:adress] = p.address
				    	@obj[:data][@obj[:size]][:zip_code] = p.zip_code
				    	@obj[:data][@obj[:size]][:city] = p.city
				    end
			    	@obj[:size] += 1
			    end
    		end
    	end
    	return @obj
	end
	
	def self.fetch(page, num, status=true, syst=false)
		@obj = {:size => 0, :data => []}
    	full_size = Subscribe.where({'category_'+num.to_s => {'$elemMatch' => {'status' => status}}}).all
    	if (full_size)
	    	tmp = Subscribe.where({'category_'+num.to_s => {'$elemMatch' => {'status' => status}}}).all

	    	full_size.each { |e| e['category_'+num].each { |z| @obj[:size] += 1 if z['status'] == status } }
	    	i = j = 0
	    	tmp.each do |p|
	    		p['category_'+num.to_s].each do |e| 
	    			if e['status'] == status && j >= ((page.to_i - 1) * 9)  && j < (page.to_i * 9)
		    			@obj[:data][i] = {:id => e['id'], :full_name => (p.first_name+' '+p.last_name).truncate(20), :profile_picture => 'https://graph.facebook.com/'+p.fb_userid+'/picture', :picture => Settings.root_path+'subscribes/picture/'+e['id'], :category => ('category_'+num.to_s).gsub("category_1", "MA TOYOTA").gsub("category_2", "DETAIL EXTERIEUR").gsub("category_3", "DETAIL INTERIEUR")}

		    			if syst && e['status'] == true
					    	@obj[:data][i][:adress] = p.address
					    	@obj[:data][i][:zip_code] = p.zip_code
					    	@obj[:data][i][:city] = p.city
					    	@obj[:data][i][:won] = e['won'] ? true : false
					    end
				    	i += 1
				    end
				    j += 1
	    		end
	    	end
		end
    	return @obj
	end

	def self.moderate(id, status)
		raw = Subscribe.where({
			'$or' => [
				{'category_1' => {'$elemMatch' => {'id' => id}}},
				{'category_2' => {'$elemMatch' => {'id' => id}}},
				{'category_3' => {'$elemMatch' => {'id' => id}}},
			]
		})

		@to_up = {}

		raw.each do |r|
			r['category_1'].each_with_index { |a, j| r['category_1'][j]['status'] = status if a['id'] == id; @to_up = r }
			r['category_2'].each_with_index { |a, j| r['category_2'][j]['status'] = status if a['id'] == id; @to_up = r }
			r['category_3'].each_with_index { |a, j| r['category_3'][j]['status'] = status if a['id'] == id; @to_up = r }
		end

		@to_up.save!
		return true
	end
end
