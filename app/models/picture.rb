class Picture
    include MongoMapper::EmbeddedDocument

    key :status, Boolean
    key :id, String
    key :device, String
    key :date, Date
end