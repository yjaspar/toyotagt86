class GT86.Routers.SubscribesRouter extends Backbone.Router
    initialize: (options) ->
        @subscribes or= new GT86.Collections.SubscribesCollection()
        @_options = options
        @data = {}
        @_options.uri = extractUrlParams()

    routes:
        "index"             : "index"
        "pushFan"           : "push_fan"
        "form"              : "form"
        "error"             : "error"
        "prizes"            : "prizes"
        ":id"               : "home"
        ".*"                : "index"

    index: ->
        if !@_options.uri['page']
            if !@_options.request.page then @dispatch() else @push_fan()
        else
            switch @_options.uri['page']
                when 'form' then Backbone.history.navigate '#/form', {trigger: true}
                when 'dotation' then Backbone.history.navigate '#/prizes', {trigger: true}
                else Backbone.history.navigate '#/'+@_options.uri['page'], {trigger: true}


    form: () ->
        self = @
        fbEnsureInit( ->
            FB.getLoginStatus (response) ->
                if response.authResponse
                    FB.api '/me', (res) ->
                        $.ajax {
                            url: '/subscribes/fbid/'+res.id+'.json'
                            dataType: 'json',
                            type: 'GET',
                            success: (data) ->
                                res.city = if data then data.city else ""
                                res.zip_code = if data then data.zip_code else ""
                                res.address = if data then data.address else ""
                                res.rules = if data then 'checked="checked"' else ''
                                res.pictures = [
                                    if data then data.category_1.length else 0,
                                    if data then data.category_2.length else 0,
                                    if data then data.category_3.length else 0
                                ]

                                $("#app div").html ''
                                $("#app center").fadeOut 'fast', () ->
                                    if self._options.device == 'mobile'
                                    # if 'mobile' == 'mobile'
                                        self.view = new GT86.Views.Subscribes.FormPhoneView(config: self._options, collection: self.subscribes, fb: res)
                                    else
                                        self.view = new GT86.Views.Subscribes.FormView(config: self._options, collection: self.subscribes, fb: res)
                                    $("#app div").html(self.view.render().el)
                        }
                else
                    FB.login((response) ->
                        if response.authResponse
                            self.form()
                        else
                            Backbone.history.navigate '#error', {trigger: true}
                    , {scope : self._options.config.facebook.app_perms })
        )

    dispatch: ->
        self = @
        $("#app div").html ''
        $("#app center").fadeOut 'fast', () ->
            if self._options.device == 'mobile'
            # if 'mobile' == 'mobile'
                self.view = new GT86.Views.Subscribes.DispatchPhoneView(config: self._options, collection: self.subscribes)
            else
                self.view = new GT86.Views.Subscribes.DispatchView(config: self._options, collection: self.subscribes)
            $("#app div").html(self.view.render().el)
    prizes: ->
        self = @
        $("#app div").html ''
        $("#app center").fadeOut 'fast', () ->
            if self._options.device == 'mobile'
            # if 'mobile' == 'mobile'
                self.view = new GT86.Views.Subscribes.PrizesPhoneView(config: self._options)
            else
                self.view = new GT86.Views.Subscribes.PrizesView(config: self._options)
            $("#app div").html(self.view.render().el)

    push_fan: ->
        self = @
        $("#app div").html ''
        $("#app center").fadeOut 'fast', () ->
            self.view = new GT86.Views.Subscribes.PushFanView(object: self._options)
            $("#app div").html(self.view.render().el)

    error: ->
        self = @
        $("#app div").html ''
        $("#app center").fadeOut 'fast', () ->
            self.view = new GT86.Views.Subscribes.ErrorView(self._options, self.subscribes)
            $("#app div").html(self.view.render().el)

    home: (id) ->
        self = @
        $("#app div").html ''
        $("#app center").fadeOut 'fast', () ->
            if self._options.device == 'mobile'
            # if 'mobile' == 'mobile'
                self.view = new GT86.Views.Subscribes.HomePhoneView(gallery: id, config: self._options, collection: self.subscribes)
            else
                self.view = new GT86.Views.Subscribes.HomeView(gallery: id, config: self._options, collection: self.subscribes)
            $("#app div").html(self.view.render().el)
