GT86.Views.Subscribes ||= {}

class GT86.Views.Subscribes.PrizesPhoneView extends Backbone.View
    template: JST["backbone/templates/subscribes/prizes_phone"]
    events: {
    }

    homePage: () =>
        top.location.href =  "https://apps.facebook.com/"+@params.config.config.facebook.app_namespace

    initialize: (@options) ->
        window.scrollTo(0, 0)
    	
    render: =>
        @$el.html @template
        return this
