GT86.Views.Subscribes ||= {}

class GT86.Views.Subscribes.HomeView extends Backbone.View
    template: JST["backbone/templates/subscribes/home"]
    events: {
        "click #previous_gallery": "previous",
        "click #next_gallery": "next",
        "click .picture_gallery": "printPicture",
        "click #join_form": "askPerms"
        'click #header a': 'homePage'
    }

    homePage: () =>
        top.location.href =  "https://apps.facebook.com/"+@params.config.config.facebook.app_namespace

    initialize: (params) =>
        $(document).scrollTop 0
        @params = params
        @gallery = params.gallery
        @loadGallery 1

    famous: () ->
        @order = 'fame'
        @loadGallery 1

    new: () ->
        @order = 'date'
        @loadGallery 1

    previous: () ->
        $('#paginate').pagination('prevPage');

    next: () ->
        $('#paginate').pagination('nextPage');

    askPerms: () ->
        FB.getLoginStatus (response) ->
            if response.authResponse
                FB.api '/me', (res) ->
                    $.fancybox.open {
                        type: 'ajax',
                        autoDimensions: false,
                        width : '320',
                        height: '500',
                        content: JST['backbone/templates/subscribes/form'] res
                    }
            else
                FB.login((response) ->
                    if response.authResponse
                        askPerms()
                    return
                , {scope : @params.config.config.facebook.app_perms })

    getDim: (img) ->
        h = img.height
        w = img.width
        if (h > w)
            h = if h > 480 then 480 else h
            w = 'auto'
            h += 90
        else
            w = if w > 480 then 480 else (if w < 200 then 200 else w)
            h = 'auto'

        return {height: h, width: w}

    printPicture: (id) ->
        self = @
        id = id.target.getAttribute('id')
        opt = {'url': '/subscribes/'+id+'.json', 'dataType': 'json'}
        opt.success = (object) ->
            load = new Image();
            load.src = object.picture;
            load.onload = () ->
                dim = self.getDim load
                $.fancybox.open {
                    autoSize: false,
                    autoDimensions: false,
                    width: dim.width,
                    height: dim.height,
                    type: 'ajax',
                    content: JST['backbone/templates/subscribes/picture'] object
                }
        $.ajax opt

    loadGallery: (current) ->
        opt = {'url': '/subscribes/'+@gallery+'/'+current+'.json', 'dataType': 'json'}
        self = @
        opt.success = (object) ->
            self.displayPictures object.data
            $('#paginate').pagination({
                items: parseInt(object.size),
                itemsOnPage: 9,
                currentPage: current,
                cssStyle: 'compact-theme',
                hrefTextPrefix: (if self.params.config.uri.page then "?page="+self.params.config.uri.page else "")+'#/'+self.gallery+'/',
                onPageClick: (page_n, e) ->
                    self.loadGallery page_n
            })
            if self.params.config.uri['_id_']
                self.printPicture(self.params.config.uri['_id_'])
        opt.error = (data) ->
            console.log(data);
            alert 'oops, une erreur s\'est produite. Rechargez votre page pour retourner sur Toyota GT86'
        $.ajax opt

    displayPictures: (p) ->
        html = ''
        gallery = @gallery
        for doc in p
            do (doc) ->
                html += "<div class='cell'>"
                html += "    <div class='cell-top'>"
                html += "        <img src='"+doc.profile_picture+"' />"
                html += "        <div class='text fgblack Rg'>"+doc.full_name+"</div>"
                html += "    </div>"
                html += "    <div style='clear: both'></div>"
                html += "    <div class='thumbnail-container'>"
                html += "        <img class='picture_gallery' id='"+doc.id+"' src='"+doc.picture+"' />"
                html += "    </div>"
                html += "</div>"

        $('#photos').html html
            


    render: ->
        @$el.html(@template {'gallery': @gallery})
        return @
