GT86.Views.Subscribes ||= {}

class GT86.Views.Subscribes.DispatchPhoneView extends Backbone.View
    template: JST["backbone/templates/subscribes/dispatch_phone"]


    homePage: () =>
        top.location.href =  "https://apps.facebook.com/"+@params.config.config.facebook.app_namespace

    initialize: (params) =>
        @params = params
        window.scrollTo(0, 0)

    render: ->
        @$el.html @template(data: @data) 
        return @
