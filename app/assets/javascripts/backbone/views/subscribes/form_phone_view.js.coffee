GT86.Views.Subscribes ||= {}

class GT86.Views.Subscribes.FormPhoneView extends Backbone.View
    template: JST["backbone/templates/subscribes/form_phone"]
    events: {
        'click #add-more-link': 'addMoreEListener',
    }

    homePage: () =>
        top.location.href =  "https://apps.facebook.com/"+@params.config.config.facebook.app_namespace

    addMoreEListener: () =>
    	Backbone.history.stop()
    	Backbone.history.start()

    initialize: (params) =>
        $(document).scrollTop 0
        @params = params

    render: ->
        @$el.html @template(@params.fb)
        return @
