GT86.Views.Subscribes ||= {}

class GT86.Views.Subscribes.GalleryView extends Backbone.View
    template: JST["backbone/templates/subscribes/gallery"]
    events: {
        
    }

    initialize: (params) ->
        $(document).scrollTop 0
        @params = params
        @gallery = params.gallery
        loadGallery 1

    printPicture: (id) ->
        FB.getLoginStatus (response) ->
            self = @
            fbid = if response.authResponse.userID then response.authResponse.userID else ''
            id = if (typeof id == 'string') then id else id.toElement.id
            opt = {'url': '/subscribes/'+id+'.json', 'dataType': 'json'}
            opt.success = (object) ->
                $.fancybox.open {
                    autoDimensions: false,
                    width : '320',
                    type: 'ajax',
                    content: JST['backbone/templates/subscribes/picture'] object
                }
            $.ajax opt

    loadGallery: (current) ->
        opt = {'url': '/subscribes/'+@gallery+'/'+current+'.json', 'dataType': 'json'}
        self = @
        opt.success = (object) ->
            self.displayPictures object.data
            $('#paginate').pagination({
                items: parseInt(object.size),
                itemsOnPage: 12,
                currentPage: current
                cssStyle: 'light-theme'
                onPageClick: (page_n, e) ->
                    self.loadGallery page_n
            })
            if self.params.config.uri['_id_']
                self.printPicture(self.params.config.uri['_id_'])
        opt.error = (data) ->
            alert 'Une erreure est survenue lors du chargement de la galerie, merci de contacter un administrateur'
        $.ajax opt

    displayPictures: (p) ->
        html = ''
        for doc in p
            do (doc) ->
                html += '<img class="picture_gallery" id="'+doc.id+'" src="'+doc.picture+'" height="136" width="136" style="float: left; margin: 10px;" />'

        $('#gallery').html html
            


    previous: () ->
        $('#paginate').pagination('prevPage');

    next: () ->
        $('#paginate').pagination('nextPage');