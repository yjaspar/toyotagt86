GT86.Views.Subscribes ||= {}

class GT86.Views.Subscribes.DispatchView extends Backbone.View
    template: JST["backbone/templates/subscribes/dispatch"]
    events: {
        'click #header a': 'homePage',
        'click .categorie a': 'categoryLink'
    }

    categoryLink: (e) ->
        Backbone.history.navigate '#/'+e.target.getAttribute('id'), {trigger: true}

    homePage: () ->
        top.location.href =  "https://apps.facebook.com/"+@params.config.config.facebook.app_namespace

    initialize: (params) ->
        @params = params
        $(document).scrollTop 0

    render: ->
        @$el.html @template(data: @data) 
        return @
