GT86.Views.Subscribes ||= {}

class GT86.Views.Subscribes.FormView extends Backbone.View
    template: JST["backbone/templates/subscribes/form"]

    events: {
        'click #add-more-link': 'addMoreEListener',
        'click #gallery-link': 'galleryLink',
        'click #popup4 #dota-bot': 'back',
        'click #dota a': 'dotation',
        'click #back a, #header a': 'homePage'
    }

    initialize: (params) =>
        $(document).scrollTop 0
        @params = params

    galleryLink: () =>
        if navigator.userAgent.match(/MSIE 9.0/)
            top.location.href = "https://apps.facebook.com/"+@params.config.config.facebook.app_namespace+"/?page=1"
        else
            Backbone.history.loadUrl '#1'

    homePage: () =>
        top.location.href =  "https://apps.facebook.com/"+@params.config.config.facebook.app_namespace

    back: () =>
        top.location.href =  "https://apps.facebook.com/"+@params.config.config.facebook.app_namespace+"/?page=1"

    dotation: () =>
        Backbone.history.navigate '#prizes', {trigger: true}

    addMoreEListener: () =>
        if navigator.userAgent.match(/MSIE 9.0/)
            top.location.href = "https://apps.facebook.com/"+@params.config.config.facebook.app_namespace+"/?page=form"
        else
        	$(document).scrollTop 0
        	Backbone.history.stop()
        	Backbone.history.start()

    render: ->
        @$el.html @template(@params.fb)
        return @
