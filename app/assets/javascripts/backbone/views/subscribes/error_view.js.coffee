GT86.Views.Subscribes ||= {}

class GT86.Views.Subscribes.ErrorView extends Backbone.View
    template: JST["backbone/templates/subscribes/error"]
    events: {
        'click #back a': 'refreshPerms',
    }

    initialize: (@options) ->
        $(document).scrollTop 0
        this.render


    refreshPerms: () ->
        Backbone.history.navigate '#form', {trigger: true}

    render: =>
        @$el.html(@template(data: @options, message: 'Pwet for the moment'))
        return this
