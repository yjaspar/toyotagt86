GT86.Views.Subscribes ||= {}

class GT86.Views.Subscribes.HomePhoneView extends Backbone.View
    template: JST["backbone/templates/subscribes/home_phone"]
    events: {
        "click #previous_gallery": "previous",
        "click #next_gallery": "next",
        "click .picture_gallery": "printPicture",
        "click #join_form": "askPerms"
    }

    homePage: () =>
        top.location.href =  "https://apps.facebook.com/"+@params.config.config.facebook.app_namespace

    initialize: (params) =>
        window.scrollTo(0, 0)
        @params = params
        @gallery = params.gallery
        @loadGallery 1

    famous: () ->
        @order = 'fame'
        @loadGallery 1

    new: () ->
        @order = 'date'
        @loadGallery 1

    previous: () ->
        $('#paginate').pagination('prevPage');

    next: () ->
        $('#paginate').pagination('nextPage');

    askPerms: () ->
        FB.getLoginStatus (response) ->
            if response.authResponse
                FB.api '/me', (res) ->
                    $.fancybox.open {
                        type: 'ajax',
                        autoDimensions: false,
                        width : '320',
                        height: '500',
                        content: JST['backbone/templates/subscribes/form'] res
                    }
            else
                FB.login((response) ->
                    if response.authResponse
                        askPerms()
                    return
                , {scope : @params.config.config.facebook.app_perms })

    getDim: (img) ->
        h = img.height
        w = img.width
        if (h > w)
            h = if h > 480 then 480 else h
            w = 'auto'
            h += 90
        else
            w = if w > 480 then 480 else (if w < 200 then 200 else w)
            h = 'auto'

        return {height: h, width: w}

    printPicture: (id) ->
        self = @
        id = id.target.getAttribute('id')
        opt = {'url': '/subscribes/'+id+'.json', 'dataType': 'json'}
        opt.success = (object) ->
            load = new Image();
            load.src = object.picture;
            load.onload = () ->
                $.fancybox.open {
                    autoDimensions: true,
                    type: 'ajax',
                    content: JST['backbone/templates/subscribes/picture'] object
                }
        $.ajax opt


    loadGallery: (current) ->
        opt = {'url': '/subscribes/'+@gallery+'/'+current+'.json', 'dataType': 'json'}
        self = @
        opt.success = (object) ->
            self.displayPictures object.data
            self.max = object.size
            if self.params.config.uri['_id_']
                self.printPicture(self.params.config.uri['_id_'])
                self.params.config.uri['_id_'] = null
        opt.error = (data) ->
            alert 'Une erreure est survenue lors du chargement de la galerie, merci de contacter un administrateur'
        $.ajax opt
    ###
    displayPictures: (p) ->
        html = ''
        gallery = @gallery
        for doc in p
            do (doc) ->
                html += "<div class='cell'>"
                html += "    <div class='cell-top'>"
                html += "        <img src='"+doc.profile_picture+"' />"
                html += "        <div class='text fgblack Rg'>"+doc.full_name+"</div>"
                html += "    </div>"
                html += "    <div style='clear: both'></div>"
                html += "    <div class='thumbnail-container'>"
                html += "        <img class='picture_gallery' id='"+doc.id+"' src='"+doc.picture+"' />"
                html += "    </div>"
                html += "</div>"

        $('#photos').html html
        ###         

    displayPictures: (p) ->
        self = @
        html = ''
        i = 0;
        for doc in p
            do (doc) ->
                name = doc.full_name.split(' ')
                doc.full_name = name[0]+' '+name[1].charAt(0)+'.'
                html += '<div class="picture">';
                html += "    <div class='cell-top'>"
                html += "        <img src='"+doc.profile_picture+"' />"
                html += "        <div class='text fgblack Rg'>"+doc.full_name+"</div>"
                html += "    </div>"
                html += '    <div class="clear"></div>';
                html += '    <div class="thumbcover" id="'+doc.id+'">';    
                html += "        <img class='picture_gallery' id='"+doc.id+"' src='"+doc.picture+"' />"
                html += '    </div>';
                html += '</div>';
                i++;
                if (i%2 == 0)
                    html += '<div class="clear"></div>';
        
        if (@page == 1) then $('#photos').html html else $('#photos').append html

        load = false
        $(window).scroll () ->
            if($('.picture:last').offset()? && $('.picture')?)
                if (self.scroll && $(window).scrollTop() + $(window).height()) >= $('.picture:last').offset().top && !load && $('.picture').length < self.max
                    self.page += 1
                    load = true
                    self.loadGallery()

    render: ->
        @$el.html(@template {'gallery': @gallery})
        return @
