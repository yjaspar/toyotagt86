GT86.Views.Subscribes ||= {}

class GT86.Views.Subscribes.PrizesView extends Backbone.View
    template: JST["backbone/templates/subscribes/prizes"]
    events: {
       	'click #header a': 'homePage'
    }

    homePage: () =>
        top.location.href =  "https://apps.facebook.com/"+@options.config.config.facebook.app_namespace

    initialize: (@options) ->
    	$(document).scrollTop 0
    	
    render: =>
        @$el.html @template
        return this
