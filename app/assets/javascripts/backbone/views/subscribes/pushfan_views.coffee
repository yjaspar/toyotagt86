GT86.Views.Subscribes ||= {}

class GT86.Views.Subscribes.PushFanView extends Backbone.View
    template: JST["backbone/templates/subscribes/push_fan"]
    events: {'click a': 'sendCanvas'}

    initialize: (data) ->
        $(document).scrollTop 0
        @data = data.object

    sendCanvas: (e) ->
    	top.location.href =  "https://apps.facebook.com/"+@data.config.facebook.app_namespace+'?page='+e.target.getAttribute 'rel'

    getLogin: () ->
        top.location.href = "https://apps.facebook.com/"+@data.config.facebook.app_namespace

    render: ->
        @$el.html @template(data: @data) 
        return @
