class GT86.Models.Subscribe extends Backbone.Model
  	paramRoot: 'subscribe'

	defaults:
	    firstname: null
	    lastname: null
	    email: null
	    birthdate: null
	    use_conditions: null
	    fb_userid: null

class GT86.Collections.SubscribesCollection extends Backbone.Collection
	model: GT86.Models.Subscribe
	url: '/subscribes'