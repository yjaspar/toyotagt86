class AdminsController < ApplicationController
	before_filter :authorize

	private
 
	def authorize
		unless session[:user_id]
			redirect_to controller: 'subscribes', action: 'index'
		end
	end

	public

	def index
		render :index, :layout => 'admin_layout'
	end

	def subscribers
		cur = (params[:page] ? params[:page] : 1)
		@subscribers = Subscribe.fetch(cur, '1', false)
		
		tmp = Subscribe.fetch(cur, '2', false)
		@subscribers[:size] += tmp[:size]
		@subscribers[:data].concat(tmp[:data])

		tmp = Subscribe.fetch(cur, '3', false)
		@subscribers[:size] += tmp[:size]
		@subscribers[:data].concat(tmp[:data])

		@subscribers[:current] = cur

		respond_to do |format|
            # format.xls { send_data @export.to_export.to_xls, disposition: 'attachment', :filename => 'Subscribes.xls' }
            format.html {render :layout => 'admin_layout'}
        end
	end

	def moderate
		Subscribe.moderate(params[:id], params[:state] == 'true' ? true : false)

		respond_to do |format|
			format.json { render json: {success: true} }
		end
	end

	def winners
		cur = (params[:page] ? params[:page] : 1)

		@subscribers = Subscribe.fetch(cur, '1', true, true)
		
		tmp = Subscribe.fetch(cur, '2', true, true)
		@subscribers[:size] += tmp[:size]
		@subscribers[:data].concat(tmp[:data])

		tmp = Subscribe.fetch(cur, '3', true, true)
		@subscribers[:size] += tmp[:size]
		@subscribers[:data].concat(tmp[:data])


		@winners = Subscribe.winners('1')
		
		tmp = Subscribe.winners('2')
		@winners[:size] += tmp[:size]
		@winners[:data].concat(tmp[:data])

		tmp = Subscribe.winners('3')
		@winners[:size] += tmp[:size]
		@winners[:data].concat(tmp[:data])

		respond_to do |format|
            # format.xls { send_data @export.to_xls, disposition: 'attachment', :filename => 'Subscribes.xls' }
            format.html {render :layout => 'admin_layout'}
        end
	end

	def grant
		raw = Subscribe.where({
			'$or' => [
				{'category_1' => {'$elemMatch' => {'id' => params[:id]}}},
				{'category_2' => {'$elemMatch' => {'id' => params[:id]}}},
				{'category_3' => {'$elemMatch' => {'id' => params[:id]}}},
			]
		})

		@to_up = {}

		raw.each do |r|
			r['category_1'].each_with_index { |a, j| r['category_1'][j]['won'] = true if a['id'] == params[:id]; @to_up = r }
			r['category_2'].each_with_index { |a, j| r['category_2'][j]['won'] = true if a['id'] == params[:id]; @to_up = r }
			r['category_3'].each_with_index { |a, j| r['category_3'][j]['won'] = true if a['id'] == params[:id]; @to_up = r }
		end

		@to_up.save!

		respond_to do |format|
			format.json { render json: {success: true} }
		end
	end

	def ungrant
		raw = Subscribe.where({
			'$or' => [
				{'category_1' => {'$elemMatch' => {'id' => params[:id]}}},
				{'category_2' => {'$elemMatch' => {'id' => params[:id]}}},
				{'category_3' => {'$elemMatch' => {'id' => params[:id]}}},
			]
		})

		@to_up = {}

		raw.each do |r|
			r['category_1'].each_with_index { |a, j| r['category_1'][j]['won'] = false if a['id'] == params[:id]; @to_up = r }
			r['category_2'].each_with_index { |a, j| r['category_2'][j]['won'] = false if a['id'] == params[:id]; @to_up = r }
			r['category_3'].each_with_index { |a, j| r['category_3'][j]['won'] = false if a['id'] == params[:id]; @to_up = r }
		end

		@to_up.save!

		respond_to do |format|
			format.json { render json: {success: true} }
		end
	end

	def notifications
		@subscribers = Subscribe.where('data.type' => 'facebook')
		render :notifications, :layout => 'admin_layout'
	end

	def users
		@admins = Admin.all
	    @_admin = Admin.new

	    render :users, :layout => 'admin_layout'
	end

	# POST /admins
	# POST /admins.json
	def create
	    @admin = Admin.new({:firstname => params['firstname'], :lastname => params['lastname'], :fb_userid => params['fb_userid']})
	    @admin.save
	    respond_to do |format|
	        format.html { redirect_to action: 'users', notice: 'Admin was successfully created.' }
	        format.json { render json: @admin, status: :created, location: @admin }
	    end
	end

	def destroy
	    @admin = Admin.find(params[:id])
	    @admin.destroy

	    respond_to do |format|
	        format.html { redirect_to action: 'users'}
	        format.json { head :no_content }
	    end
	end

	##
	## STATS
	##

	def get_stats
        @p = Subscribe.all
        @date_order = {}
        @p.each do |s|
        	s.category_1.each { |a| @date_order[a['date'].year.to_s+'-'+a['date'].month.to_s] = [] if !@date_order[a['date'].year.to_s+'-'+a['date'].month.to_s]; @date_order[a['date'].year.to_s+'-'+a['date'].month.to_s].push(:category => 'category_1', :status => a['status'], :device => a['device'])}
        	s.category_2.each { |a| @date_order[a['date'].year.to_s+'-'+a['date'].month.to_s] = [] if !@date_order[a['date'].year.to_s+'-'+a['date'].month.to_s]; @date_order[a['date'].year.to_s+'-'+a['date'].month.to_s].push(:category => 'category_2', :status => a['status'], :device => a['device'])}
        	s.category_3.each { |a| @date_order[a['date'].year.to_s+'-'+a['date'].month.to_s] = [] if !@date_order[a['date'].year.to_s+'-'+a['date'].month.to_s]; @date_order[a['date'].year.to_s+'-'+a['date'].month.to_s].push(:category => 'category_3', :status => a['status'], :device => a['device'])}
        end
        
        respond_to do |format|
            format.json { render json: @date_order }
        end
    end
end