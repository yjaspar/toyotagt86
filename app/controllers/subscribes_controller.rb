class SubscribesController < ApplicationController
	skip_before_filter :verify_authenticity_token, :only => [:create]
    # before_filter :authorize, :except => [:index, :create, :get_by_fbid, :wrapper]

    def index
        require 'facebook'

        logger.info(params[:signed_request])
        @request = params[:signed_request] ? Facebook::Parsor.signed_request(params[:signed_request], Settings.facebook.app_secret) : session[:request]
        logger.info(@request)
        @admin = Admin.find_by_fb_userid((@request["user_id"] if @request && @request.has_key?("user_id")))
        @conf = {
            request: {
                user_id: (@request['user_id'] if @request && @request.has_key?("user_id")),
                page: (@request['page'] if @request && @request.has_key?("page"))
            }, config: {
                facebook: {
                    app_id: Settings.facebook.app_id,
                    app_namespace: Settings.facebook.app_namespace,
                    app_perms: Settings.facebook.app_perms
                }
            },
            device: request.env['mobvious.device_type'].to_s
        }

        if @admin
            session[:user_id] = @admin.id
            session[:request] = @request
        end
        
        respond_to do |format|
            format.html # index.html.erb
        end
    end

    def get_by_fbid
        @subscribe = Subscribe.find_by_fb_userid(params[:id])
        respond_to do |format|
            format.json { render json: @subscribe }
        end

    end

    def form
        @subscribe = Subscribe.find_by_fb_userid(params[:fbid])
        if !@subscribe
            @subscribe = Subscribe.new(
                :fb_userid => params[:fbid],
                :last_name => params[:last_name],
                :first_name => params[:first_name],
                :email => params[:email],
                :address => params[:address],
                :zip_code => params[:zip_code],
                :city => params[:city])
        end
        
        @subscribe.category_1 << Picture.new(:status => false, :id => params[:picture_id_1], :device => request.env['mobvious.device_type'].to_s, :date => Time.now).to_mongo if params[:picture_id_1] != ""
        @subscribe.category_2 << Picture.new(:status => false, :id => params[:picture_id_2], :device => request.env['mobvious.device_type'].to_s, :date => Time.now).to_mongo if params[:picture_id_2] != ""
        @subscribe.category_3 << Picture.new(:status => false, :id => params[:picture_id_3], :device => request.env['mobvious.device_type'].to_s, :date => Time.now).to_mongo if params[:picture_id_3] != ""

        @subscribe.save!
        
        respond_to do |format|
            format.json { render json: {success: true} }
        end
    end

    def picture
        require 'mongo'
        require 'bson'

        @db = Mongo::MongoClient.new(Settings.db[Rails.env].host, Settings.db[Rails.env].port).db(Settings.db[Rails.env].name)
        @db.authenticate(Settings.db[Rails.env].auth.login, Settings.db[Rails.env].auth.password) if Settings.db[Rails.env].auth
        @grid = Mongo::Grid.new(@db)
        @data = @grid.get(BSON::ObjectId(params[:id]))

        send_data(@data.read, {
            :filename => @data.filename,
            :disposition => 'inline',
            :type => @data.content_type
        })   
    end

    def get
    	@subscribe = Subscribe.get(params[:id])
    	respond_to do |format|
    		format.json { render json: @subscribe }
    	end
    end

    def create
        require 'mongo'
        require 'mime/types'

        @db = Mongo::MongoClient.new(Settings.db[Rails.env].host, Settings.db[Rails.env].port).db(Settings.db[Rails.env].name)
        @db.authenticate(Settings.db[Rails.env].auth.login, Settings.db[Rails.env].auth.password) if Settings.db[Rails.env].auth
        @grid = Mongo::Grid.new(@db)

        @image = MiniMagick::Image.read(params[:Filedata].tempfile)
        if (request.env['HTTP_USER_AGENT'].downcase.match(/iphone|ipad/i))
            if(@image["EXIF:Orientation"]=='6')
                @image.combine_options do |c|
                    c.rotate "+90>"
                end
            elsif(@image["EXIF:Orientation"]=='3')
                @image.combine_options do |c|
                    c.rotate "+180>"
                end
            end
        end

        id = @grid.put(@image.to_blob(), :filename => params[:Filedata].original_filename, :content_type => MIME::Types.type_for(params[:Filedata].original_filename).first.to_s)
        respond_to do |format|
            format.json { render json: {:file_id => id} }
        end
    end

    def fetch
    	@obj = Subscribe.fetch(params[:page], params[:pic], params[:status] ? params[:status] : true)
    	respond_to do |format|
            format.json { render json: @obj }
        end
    end
end
